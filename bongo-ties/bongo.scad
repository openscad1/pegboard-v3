    // NOSTL

include <../lib/libopenscad/mcube.scad>;

bongo_length = 60; // [40, 60, 80]

bongo_endcap_diameter = bongo_length / 2;


default_scale = 1.00;
default_vscale=1.25;

//debug = true;


module bongo(h = bongo_length, w = bongo_endcap_diameter, scale = default_scale, vscale = default_vscale) {

    module knob() {
        scale([1,1,scale])  {
            difference() {
                hull() {
                    
                    // "outer" face
                    cube([w, w/2, w/2], center = true);
                    cube([w/2, w, w/2], center = true);
   
                    // "inner" face
                    translate([0, 0, -w * .5]) hull() {
                        cube([w/4, w/2, 0.01], center = true);
                        cube([w/2, w/4, 0.01], center = true);
                    }
                }

                // recess the ends
                color("red") {
                    rotate([0,0,22.5])
                    hull() {
                        translate([0, 0, w/8]) {
                            cylinder(d = w/3, h = 0.01, $fn = 8, center = true);
                        }
                        translate([0, 0, w/4 + 0.01]) {
                            cylinder(d = w * 0.8, h = 0.01, $fn = 8, center = true);
                        }
                    }
                }
            }
        }

    }
    


    difference() {
        union() {
            rotate([0,90,0]) {

                // knobs
                //color("orange") 
                translate([0, 0, -h/2]) rotate([180, 0, 0]) knob();
                color("salmon") translate([0, 0, h/2]) knob();

                // axle
                scale([vscale, vscale, 1]){
                    difference() {


                        color("fuchsia") union() {
                            hull() {
                                cube([w/4, w/2, h], center = true);
                                cube([w/2 , w/4, h], center = true);
                                
                            }
                        }
                        

                    }
                }
            }
        }

        // label
        union() {
            translate([0, 0, ((w / 4) * vscale) - 0.5]) {
                linear_extrude(4) {
                    text(str(w, "x",h), halign = "center", valign = "center", size = (w/5) * vscale);
                }
            }
        }
    }
}



debug=true;
if(!is_undef(debug) && debug) {
    bongo(h=40, w=20);
}
   


