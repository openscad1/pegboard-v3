// NOSTL

include <../lib/libmos/libchamfer.scad>

fine = true; 
//fine = false; 

$fn = fine ? 50 : 12;

roundcorner = 1; 

horn_arc = 90;

horn_diameter = 6;

base_size = 25.4;

plate_thickness = 25.4 / 8;  // 1/8 inch
one_eighth = 25.4 / 8;  // 1/8 inch

trim_thickness = 0.2;
trim_width = 0.5;



// v2 parameters

width_relief = 0.25;
thickness_relief = 0.3;
overlap = 0.01;

module pad(color = "blue", thickness = one_eighth) {
    color(color) {
        translate([(base_size / 8) + roundcorner, (base_size / 8) + roundcorner, roundcorner]) {
            minkowski() {
                cube([((base_size / 8) * 4) - (roundcorner * 2), ((base_size / 8) * 4) - (roundcorner * 2), (2 * plate_thickness) - (roundcorner * 2)]);
                sphere(d = roundcorner);
            }
        }
    }


}


module pad_v2(color = "lightblue", thickness = one_eighth){
    
    x = base_size * (4/8);
    y = base_size * (4/8);
    z = thickness * 4;
    c = z / 4.75;
    
    color(color) {
        translate([base_size * (3/8), base_size * (3/8), thickness_relief]) //deleteme
        intersection() {
        translate([0, 0, thickness * 1.75 - overlap]) {
            chamfered_box([x - width_relief * 4, y - (width_relief * 4), z], chamfer = c, align = [0, 0, -1]);
        }
        color("red") {
            translate([0, 0, (thickness + thickness_relief) * 2])
            cube([x, y, z], center = true);
        }
    }
    }
    
}


module horn(tilt = 0) {
    intersection() {
    translate([(base_size / 2) - one_eighth, (base_size / 2) - one_eighth, 2 * plate_thickness]) {
        rotate([0, tilt, 180]) {
            translate([10, 0, 0]) {
                color("green") rotate([90, 180 + 45, 0]) {
                    rotate_extrude(angle=horn_arc) {
                        translate([10, 0]) {
                            circle(d = horn_diameter);
                        }
                    }
                    translate([10,0, 0]) {
                        sphere(d = horn_diameter);
                    }
                }
            }
        }
    }
    color("red") {
        translate([0, 0, one_eighth/2])
        cube([base_size, base_size, base_size]);
    }
}
}

module horn_v2(tilt = 0) {
}



module base(color = "purple", thickness = one_eighth, radius = 2) {
    color(color) {
        translate([roundcorner, roundcorner, roundcorner]) {
            trim_shim = 0.1 + trim_thickness;
            translate([0, 0, trim_shim]){
                minkowski() {
                    fudge = 0;
                    hull() {
                        translate([                                            radius , ((base_size / 8) * 6) - (roundcorner * 2) - radius - (trim_width / 2), 0]) 
                            cylinder(h = thickness - (roundcorner * 2) - trim_shim + fudge, r = radius);
                        translate([((base_size / 8) * 7) - (roundcorner * 2) - radius, ((base_size / 8) * 6) - (roundcorner * 2) - radius - (trim_width / 2), 0]) 
                            cylinder(h = thickness - (roundcorner * 2) - trim_shim + fudge, r = radius);
                        translate([                                            radius,                                             radius + (trim_width / 2), 0]) 
                            cylinder(h = thickness - (roundcorner * 2) - trim_shim + fudge, r = radius);
                        translate([((base_size / 8) * 7) - (roundcorner * 2) - radius,                                             radius + (trim_width / 2), 0]) 
                            cylinder(h = thickness - (roundcorner * 2) - trim_shim + fudge, r = radius);
                    }
                    sphere(d = roundcorner * 2);
                }

            }
        }
    }
}


module base_v2(color = "mediumpurple", thickness = one_eighth, radius = 2) {
    
    //module chamfered_box(dim = [0, 0, 0], align = [0, 0, 0], chamfer = 0, center = false) {
    
    x = base_size * (7/8);
    y = base_size * (6/8);
    z = thickness - thickness_relief;
    c = z / 4;
    
    color(color) {
        translate([0, width_relief, thickness_relief]) //deleteme
        chamfered_wall([x, y - (width_relief * 2), z], chamfer = c, align = [1, 1, 1]);
    }
}


module wall_control_horn() {
    horn(5);
}

module gatorboard_horn() {
    horn(0);
}


rotate([90, 0, 0]) {


//base();
base_v2();


//pad();
pad_v2();



// wall_control_horn();
gatorboard_horn();

}
