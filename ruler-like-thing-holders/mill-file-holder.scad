// BOF

include <ruler-like-thing-holder.scad>;

height_factor = 5.2;
height_factor2 = 6;
length_factor = 6;
width_factor = 1;

width_scale = 0.7;


intersection() {
    union() {
        // the whole thing
        
        difference() {
            union() {
                // holder
                rotate([-30, 0, 0]) {
                    translate([0, -base_size * 5 +1 , 1]) { 
                        render_ruler_holder(render_mount = false);
                    }
                }
            }

            union() {
                // intersection volume
                translate([0, wall_thickness / 2, 0]) {
                    mcube([base_size * (width_factor + 1),base_size * (length_factor * 2), base_size * (height_factor2 * 2)], align = [0, -1, 1]);
                }
                translate([0, 0, height_factor2 * base_size]) {
                    mcube([base_size * (width_factor + 1),base_size * (length_factor * 2), base_size * 2], align = [0, 0, 1]);
                }
            }
        }


        color("lightblue") {
            rail(height = height_factor2, copies = floor(width_factor), side_wall_gap = default_back_wall_gap, chamfer = chamfer);
        }
    }

    color("cyan") mcube([100, 100, 30], align = [0, 0, 0]);
    
}

slug = true;
if(slug) {
    color("red") {
        translate([0, wall_thickness, wall_thickness])
        rotate([-30, 0, 0]) {
       //     mcube([5, 20, 200], align = [0, 1, 1]);
        }
    }
}

// EOF
