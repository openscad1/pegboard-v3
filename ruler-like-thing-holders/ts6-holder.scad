// BOF

include <../lib/libmos/libchamfer.scad>;
include <../lib/libpegboard/defaults.scad>;
include <../lib/libpegboard/rail.scad>;

include <ruler-like-thing-holder.scad>;
height_factor = 6.75;
length_factor = 2.5;
width_factor = 1;

width_scale = 0.65;

render_ruler_holder();

slug_x = 8;
slug_y = 51;
slug_z = 25.4 * 3.5;
//color("red") translate([0,  slug_y/2 + (25.4/4), slug_z/ 2 + 25.4/8]) cube([slug_x, slug_y, slug_z], center = true);

// EOF
