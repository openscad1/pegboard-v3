// BOF

include <../lib/libmos/libchamfer.scad>;
include <../lib/libpegboard/defaults.scad>;
include <../lib/libpegboard/rail.scad>;

slug_x = 46;
slug_y = 22;
slug_z = 46;

gap = wall_thickness/2;

height_factor = 1;
rail_height = 2;

width_factor = (slug_x + (wall_thickness + gap) * 2) / base_size;
length_factor = (slug_y + (wall_thickness + gap) * 2) / base_size;


height = base_size * height_factor;

outside_length = base_size * length_factor;
inside_length = outside_length - (wall_thickness * 2);

outside_width = (base_size * width_factor) - default_back_wall_gap;
inside_width = outside_width - (wall_thickness * 2);

chamfer = wall_thickness / 4;

inside_radius = wall_thickness * 1;


$fn = 200;


// true
// false

label = true;
slug = false;





module body() {
    difference() {    
        union() {
            translate([0, outside_length / 2 + 0.01, 0.005]) {  // 0.005 ... wth?
                translate([0, 0, height / 2]) {
                    radiused_box(inside_height = height, inside_length = inside_length, inside_width = inside_width, inside_radius = inside_radius, wall_thickness = wall_thickness, color = "pink", chamfer = chamfer);
                }
                translate([0, 0, wall_thickness / 2]) {
                    hull() {
                        radiused_box(inside_height = wall_thickness, inside_length = inside_length, inside_width = inside_width, inside_radius = inside_radius, wall_thickness = wall_thickness, color = "pink", chamfer = chamfer);
                    }
                }
            }
        }    
        if(label) {
            color("cyan") {
                translate([0, outside_length / 2, -wall_thickness / 4]) {
                    rotate([0, 0, 0]) {
                        linear_extrude(wall_thickness /2) {
                            mirror([0, 1, 0]) {
                                fontsize = 5;
                                text(str("L", floor(length_factor * 100) / 100, "  H", floor(height_factor * 100) / 100, "  W", floor(width_factor * 100) / 100), valign = "center", halign = "center", size = fontsize);
                            }
                        }
                    }
                }
            }
        }
    }
}

module mount() {
    color("lightblue") {
        rail(height = rail_height, copies = floor(width_factor), side_wall_gap = default_back_wall_gap, chamfer = chamfer);
    }
}



if(slug) {
    color("red")
    translate([0, (length_factor * base_size) / 2, slug_z / 2 + wall_thickness+ gap])
    cube([slug_x, slug_y, slug_z], center = true);
}


intersection() {
    color("blue") {
      //  cube([100, 100, 100]);
    }

    difference () {
        union() {
            body();
            mount();
        }


        color("red") {
            translate([0, 0, -499.99]) {
                cube([1000, 1000, 1000], center = true);
            }
        }
    }

}

// EOF
