include <../lib/libpegboard/defaults.scad>;
include <../lib/libpegboard/rail.scad>;

include <../lib/libopenscad/mcube.scad>;
include <../lib/libopenscad/mbox.scad>;

one_inch = 25.4;

module slug_4x4() {
    x = one_inch * 4;
    y = one_inch / 8;
    z = one_inch * 4;

    color("cyan") {
        mcube([x,y,z], align = [0, 0, 1]);
    }
    
}



slug = true;

if(!is_undef(slug)) {
    rotate([default_forward_lean_angle, 0, 0]) {
 //   slug_4x4();
    }
}



default_forward_lean_angle = -30;
default_grid_height_ratio = 0.5;
default_gap = one_inch / 16;
default_wall_thickness = wall_thickness;


module omnigrid_rack(grid_height, grid_depth, grid_width, gap = default_gap, grid_height_ratio = default_grid_height_ratio, forward_lean_angle = default_forward_lean_angle, wall_thickness = default_wall_thickness) {

    // pocket
    color("red") 
    rotate([forward_lean_angle, 0, 0]) {
   //     mcube([grid_width + (gap * 2), wall_thickness + (gap * 2), grid_height * grid_height_ratio], align = [0, 0, 1]);
    }
    


    module plusx_plusz() {
        // +x, +z 
        translate([(grid_width/2) + gap + (wall_thickness / 2), 0, (grid_depth + gap) / sin(forward_lean_angle)]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, grid_height * grid_height_ratio], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }

    }
    
    module minusx_plusz() {
        // -x, +z 
        translate([-((grid_width/2) + gap + (wall_thickness / 2)), 0, (grid_depth + gap) / sin(forward_lean_angle)]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, grid_height * grid_height_ratio], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }

    }


    module plusx_minusz() {
        // +x, -z 
        translate([(grid_width/2) + gap + (wall_thickness / 2), 0, -((grid_depth + gap) / sin(forward_lean_angle))]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, grid_height * grid_height_ratio], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }

    }
    
    module minusx_minusz() {
        // -x, -z 
        translate([-((grid_width/2) + gap + (wall_thickness / 2)), 0, -((grid_depth + gap) / sin(forward_lean_angle))]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, grid_height * grid_height_ratio], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }

    }

    // top plate
    color("lightgreen") {
        hull() {
            plusx_plusz();
            minusx_plusz();
        }
    }
    
    // lower plate
    color("lightblue") {
        hull() {
            plusx_minusz();
            minusx_minusz();
        }
    }
    
    // right plate
    color("orange") {
        hull() {
            plusx_plusz();
            plusx_minusz();
        }
    }

    // left plate
    color("salmon") {
        hull() {
            minusx_plusz();
            minusx_minusz();
        }
    }
    
    // back wall 1 
    /*
    color("grey") {
        hull() {
            translate([0, 0, -((grid_depth + gap) / sin(forward_lean_angle))]) {
                mcube([grid_width + (gap * 2) + wall_thickness * 2, wall_thickness, wall_thickness], align = [0, 0, 0], chamfer = wall_thickness / 4);
            }
            translate([0, 0, ((grid_depth + gap) / sin(forward_lean_angle))]) {
                mcube([grid_width + (gap * 2) + wall_thickness * 2, wall_thickness, wall_thickness], align = [0, 0, 0], chamfer = wall_thickness / 4);
            }
        }
    }
    */
    
    // back wall 2
    color("grey")
    hull() {
        // +x, +z 
        translate([(grid_width/2) + gap + (wall_thickness / 2), 0, (grid_depth + gap) / sin(forward_lean_angle)]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, wall_thickness], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }

    
    
        // -x, +z 
        translate([-((grid_width/2) + gap + (wall_thickness / 2)), 0, (grid_depth + gap) / sin(forward_lean_angle)]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, wall_thickness], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }

    


        // +x, -z 
        translate([(grid_width/2) + gap + (wall_thickness / 2), 0, -((grid_depth + gap) / sin(forward_lean_angle))]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, wall_thickness], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }

    
    
        // -x, -z 
        translate([-((grid_width/2) + gap + (wall_thickness / 2)), 0, -((grid_depth + gap) / sin(forward_lean_angle))]) {
            rotate([forward_lean_angle, 0, 0]) {
                mcube([wall_thickness, wall_thickness, wall_thickness], align = [0, 0, 1], chamfer = wall_thickness / 4);
            }
        }
    }
    


}
translate([0, 0, -1 * ((one_inch / 8 + default_gap) / sin(default_forward_lean_angle)) + (wall_thickness / 2)]) {

    omnigrid_rack(grid_height = 4 * one_inch, grid_depth = one_inch / 8, grid_width = 4 * one_inch);


    translate([0, 0, -2 * ((one_inch / 8 + default_gap) / sin(default_forward_lean_angle))]) {
        omnigrid_rack(grid_height = 4 * one_inch, grid_depth = one_inch / 8, grid_width = 4 * one_inch);
        
        translate([0, 0, -2 * ((one_inch / 8 + default_gap) / sin(default_forward_lean_angle))]) {
            omnigrid_rack(grid_height = 4 * one_inch, grid_depth = one_inch / 8, grid_width = 4 * one_inch);
            
            translate([0, 0, -2 * ((one_inch / 8 + default_gap) / sin(default_forward_lean_angle))]) {
                omnigrid_rack(grid_height = 4 * one_inch, grid_depth = one_inch / 8, grid_width = 4 * one_inch);
            }
        }

    }
}

translate([0, -1, 0]) {
    rail(height = 3, chamfer = wall_thickness / 4, copies = 4);
}