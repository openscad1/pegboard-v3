// BOF

include <boxy-thing-holder.scad>;

$fn = 90;

do_box = true;

if(!is_undef(do_box)) {
    union() {
        // box
        translate([0, 0.205, 0]) {
            rail(height = 3, copies = 2, chamfer = default_pegboard_chamfer);
        }

        difference() {
            
z = 19;
z2 = 37;
            
            union() {
                difference() {
                    boxy_thing(inside_width = 57 + wall_thickness * 2, inside_length = 18, inside_height = 31 /* bottom rim */ + 47 /* window section */ + 8 /* upper rim */, inside_radius = wall_thickness);
            
            

                    translate([0, 18 + (wall_thickness * 1.5), z + 5]) {
                        
                        mcube([47, wall_thickness * 1.01, 57 - 10], align = [0, 0, 1]);

                    }
                }
                
                translate([0, 18 + (wall_thickness * 1.5), z ]) {
                    
                    window(outer_width = 47, inner_width = 40, 
                           outer_height = 57, inner_height = z2,
                           align = [0, 0, 1], interior_roundcorner_radius = 5);



                }
            }

            translate([0, 0.9, 0]) {
                mcube([base_size * 2, 10, base_size * 3], align = [0, -1, 1], color = "red");
            }
        }
    }
}





module window(outer_width = 40, inner_width = 30, outer_height = 60, inner_height = 50, wall_thickness = wall_thickness, align = [0, 0, 0], interior_roundcorner_radius = 5) {
    
    color("cyan") 
    translate([((outer_width + wall_thickness) / 2) * align.x, (wall_thickness / 2) * align.y, ((outer_height + wall_thickness) / 2) * (align.z)]) {

        minkowski() {

            difference() {
                // frame

                mcube([outer_width, 0.01, outer_height], center = true);


                if(interior_roundcorner_radius == 0) {
                    // square cutout
                    color("red")
                        mcube([inner_width + wall_thickness * 2, 1, inner_height + wall_thickness * 2], center = true);
                } else {
                    // round-cornered cutout
                    hull() 
                        rotate([90, 0, 0]) {
                            for(x = [-1, 1]) {
                                for(y = [-1, 1]) {
                                    translate([x * (inner_width / 2 - interior_roundcorner_radius + wall_thickness), y * (inner_height / 2 - interior_roundcorner_radius + wall_thickness), 0]) {
                                        color("cyan")
                                            cylinder(r = interior_roundcorner_radius, h = 2, center = true);
                                    }
                                }
                            }
                        }


                }
            }
            
            

            // minkowski object
            hull() {
                cubiness = wall_thickness;
                mcube([cubiness    , cubiness / 2, cubiness / 2], center = true);
                mcube([cubiness / 2, cubiness    , cubiness / 2], center = true);
                mcube([cubiness / 2, cubiness / 2, cubiness    ], center = true);
            }


       }
        
        
    }

}


// EOF
