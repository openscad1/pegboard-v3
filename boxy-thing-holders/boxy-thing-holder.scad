// BOF
// NOSTL


include <../lib/libpegboard/rail.scad>;
include <../lib/libpegboard/defaults.scad>;

include <../lib/libopenscad/mcube.scad>;
include <../lib/libopenscad/mbox.scad>;
include <../lib/libopenscad/pegboard-defaults.scad>;



module boxy_thing(inside_height = 60, inside_width = 40, inside_length = 20, inside_radius = wall_thickness, wall_thickness = wall_thickness, chamfer = default_pegboard_chamfer, floor = true, wall = true, align = [0, 1, 1]) {
    
    // wall
    if(wall) {
        radiused_box(inside_height = inside_height, inside_length = inside_length, inside_width = inside_width, inside_radius = inside_radius, wall_thickness = wall_thickness, color = "pink", chamfer = chamfer, align = align);
    }

    //
    if(floor) {
        hull() {
            radiused_box(inside_height = wall_thickness, inside_length = inside_length, inside_width = inside_width, inside_radius = inside_radius, wall_thickness = wall_thickness, color = "pink", chamfer = chamfer, align = align);
        }
    }
}



// EOF
